# Materials and instructions

This repository contains all materials and instructions that will be useful for you during Pivorak Ruby Summer Course 2020

## Table of Contents

### Pre-requirements

Please check the next materials before you'll proceed with your first homework:
- [Pivorak Summer Course Rules](https://docs.google.com/document/d/1jVejCFkjgThrO-3jynBmP63sF_EyCl8d6OFX7mH5D7k/edit)
- [Register Gitlab account](https://gitlab.com/users/sign_up)
- [How To Ask Questions The Smart Way](http://www.catb.org/~esr/faqs/smart-questions.html)
- [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)
- [How to Name a Branch](https://gist.github.com/digitaljhelms/4287848)
- [How to Open Correct Merge Request: VIDEO](https://youtu.be/YVWWx1BVVgQ)
- [How to keep your fork up to date with its origin](https://about.gitlab.com/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/)

### Lectures agenda
| Lecture slides | Homework | Additional materials |
|-----------------|:-------------|:---------------:|
| Intro  | N/A |  pending |
| Ruby Basics | N/A |  pending |
| Ruby OOP | N/A |  pending |
| Rails Console & DB  | N/A |  pending |
| Rails test  | N/A |  pending |
| Rails API  | N/A |  pending |
| Design Patterns | N/A |  pending |

### How to work with homeworks via Gitlab
In order to simplify process of Code Review we'll use gitlab groups.
Each of you received your own unique [gitlab group](https://docs.gitlab.com/ee/user/group/) for RSC 2020 homeworks.

In order to start work on your homework please open specific repository from [homeworks](https://gitlab.com/pivorak-rsc-2020/homeworks) group:
![Open homeworks folder](http://files.g3d.codes/Screen-Shot-2019-06-03-19-33-25-1559579683.png)

When you will open your repository please fork it into your private homework group:
![Click fork button](http://files.g3d.codes/Screen-Shot-2019-06-03-19-38-09-1559579922.png)
![Select your homework gitlab group](http://files.g3d.codes/Screen-Shot-2019-06-03-19-40-40-1559580097.png)

After fork feel free to work on your homework!

Don't implement your solution on master branch in your fork. Checkout to new one - name does not matter.
When your solution is ready create Merge request against master branch in your fork and invite lecturer to review your Merge request
[How to create correct Merge Request](https://youtu.be/YVWWx1BVVgQ)

The faster you do this - the more time you will have to improve your work and get higher score

In case if something will change on origin branch. To update your fork - follow up instructions in this article:
[How to keep your fork up to date with its origin](https://about.gitlab.com/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/)
